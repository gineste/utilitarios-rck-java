/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitariosrck;

import Entidades.CupomFiscal;
import Entidades.ItensCupom;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.jdom2.JDOMException;

/**
 *
 * @author Rodrigo
 */
public class Principal extends javax.swing.JFrame {
    
    private JFileChooser procuraArqOrigem;
    private JFileChooser procuraArqDestino;
    private JFileChooser procuraArqEspelho;
    
    private File arquivoOrigem;
    private File arquivoDestino;
    private File arquivoEspelho;
    
    Msg msg = new Msg(this, true);
    
    private BufferedReader reader;
    private InputStreamReader inputStream;
    private File file;
    
    private BufferedWriter writer;
    private OutputStreamWriter outputStream;
    
    private String codMunicipio;
    
    private ArrayList<String> linhas;
    
    private ArrayList<CupomFiscal> listaCupons;
    
    FileFilter filtroXml = new FileFilter() {
         public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            } else if (f.getName().endsWith(".xml")) {
                return true;
            } else {
                return false;
            }
        }

        public String getDescription() {
            return "xml";
        }
    };
    
    public Principal() {
        initComponents();
        this.setLocationRelativeTo(null);
        
        //faz leitura do arquivo de configurações iniciais
        try {
            procuraArqOrigem = new JFileChooser();  
            procuraArqDestino = new JFileChooser();  
            procuraArqEspelho = new JFileChooser(); 
            
            procuraArqEspelho.setDialogTitle("Selecione o arquivo xml do Espelho");
            procuraArqEspelho.setFileFilter(filtroXml);
            
            file = new File("appsped.ini");
            inputStream = new InputStreamReader(new BufferedInputStream(new FileInputStream(file)), "ISO-8859-1");
            reader = new BufferedReader(inputStream);

            String linhaConfig = null;
            boolean bCarregaMunicipios=false;
            
            if (!file.exists()) {
                JOptionPane.showMessageDialog(this, "Arquivo de configuração não encontrado [appsped.ini].", "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
            } else {
                while ((linhaConfig = reader.readLine()) != null) {
                    if (linhaConfig.contains("=") && linhaConfig.substring(0, linhaConfig.indexOf("=")).equals("PATH_DEFAULT")) {
                        String path_default = linhaConfig.substring(linhaConfig.indexOf("=")+1, linhaConfig.length());
                        procuraArqOrigem.setCurrentDirectory(new File(path_default));
                        procuraArqDestino.setCurrentDirectory(new File(path_default));
                        procuraArqEspelho.setCurrentDirectory(new File(path_default));
                    }
                    if (linhaConfig.contains("</MUNICIPIOS>")) 
                        bCarregaMunicipios=false;

                    if (bCarregaMunicipios)
                        comboMunicipio.addItem(linhaConfig);
                    
                    if (linhaConfig.contains("<MUNICIPIOS>")) 
                        bCarregaMunicipios=true;
                }

                if (reader != null) {
                    reader.close();
                }
            }
        } catch (FileNotFoundException ex) {
            msg.mensagem("ERRO GRAVE", "Erro ao iniciar aplicação", ex.getMessage());
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            msg.mensagem("ERRO GRAVE", "Erro ao iniciar aplicação", ex.getMessage());
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        txtOrigem = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtDestino = new javax.swing.JTextField();
        btnOrigem = new javax.swing.JButton();
        btnDestino = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        comboMunicipio = new javax.swing.JComboBox<>();
        btnProcessar = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        lblProgresso = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtEspelho = new javax.swing.JTextField();
        btnEspelho = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Principal - Correção do Arquivo SPED");

        jLabel1.setText("Arquivo Origem");

        jLabel2.setText("Arquivo Destino");

        btnOrigem.setText("Procurar");
        btnOrigem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrigemActionPerformed(evt);
            }
        });

        btnDestino.setText("Procurar");
        btnDestino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDestinoActionPerformed(evt);
            }
        });

        jLabel3.setText("Municipio");

        btnProcessar.setText("PROCESSAR");
        btnProcessar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessarActionPerformed(evt);
            }
        });

        lblProgresso.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProgresso.setText("Iniciando leitura do arquivo...");
        lblProgresso.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblProgresso.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblProgresso.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        jLabel4.setText("Arquivo Espelho");

        btnEspelho.setText("Procurar");
        btnEspelho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEspelhoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProcessar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblProgresso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtOrigem, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
                            .addComponent(txtDestino, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtEspelho, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboMunicipio, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnOrigem)
                            .addComponent(btnDestino)
                            .addComponent(btnEspelho))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOrigem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(btnOrigem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtDestino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDestino))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtEspelho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEspelho))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboMunicipio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnProcessar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblProgresso)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOrigemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOrigemActionPerformed
        if (procuraArqOrigem.showDialog(this, "Selecionar") != JFileChooser.APPROVE_OPTION) {
            return;
        }
        
        arquivoOrigem = procuraArqOrigem.getSelectedFile();
        arquivoDestino = new File(arquivoOrigem.toString().replaceAll(".txt", "_new.txt"));
        
        txtOrigem.setText(arquivoOrigem.toString());
        txtDestino.setText(arquivoDestino.toString());
    }//GEN-LAST:event_btnOrigemActionPerformed

    private void btnDestinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDestinoActionPerformed
        if (procuraArqDestino.showDialog(this, "Selecionar") != JFileChooser.APPROVE_OPTION) {
            return;
        }
        
        arquivoDestino = procuraArqDestino.getSelectedFile();
        
        txtDestino.setText(arquivoDestino.toString());
    }//GEN-LAST:event_btnDestinoActionPerformed

    private void btnProcessarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessarActionPerformed
        arquivoOrigem = new File(txtOrigem.getText());
        arquivoDestino = new File(txtDestino.getText());
        arquivoEspelho = new File(txtEspelho.getText());
        int percentProgress = 0;
        int iLinha = 0;
        int iContTotal = 1;
        int iCont0200 = 1;
        int iCont9900 = 1;
        
        boolean bAtualizaValores=false;
        boolean bVerificaEspelho=true;
        
        CupomFiscal cupomDesconto = new CupomFiscal();
        
        String[] aLinha;
        ArrayList<String> listaC470 = new ArrayList();
        String sLinha = "";
        
        codMunicipio = comboMunicipio.getSelectedItem().toString();
        codMunicipio = codMunicipio.substring(0, codMunicipio.indexOf(" - "));
        
        if (!arquivoEspelho.exists()) {            
            int iResp = JOptionPane.showConfirmDialog(this, "Arquivo Espelho não encontrado, deseja continuar assim mesmo?", "CONFIRMAÇÃO", JOptionPane.YES_NO_OPTION);
            if (iResp==1)
                return;
        }
        
        if (arquivoOrigem.exists()) {
            try {
                inputStream = new InputStreamReader(new BufferedInputStream(new FileInputStream(arquivoOrigem)), "ISO-8859-1");
                reader = new BufferedReader(inputStream);
                
                outputStream = new OutputStreamWriter(new FileOutputStream(arquivoDestino));
                writer = new BufferedWriter(outputStream);
                
                this.linhas = new ArrayList<>();
                
                //faz leitura do arquivo XML e carrega informações do espelho
                LeitorXML leitorXml = new LeitorXML();
                if (bVerificaEspelho)
                    this.listaCupons = leitorXml.carregaEntidades(arquivoEspelho);

                //faz a leitura geral do arquivo
                while ((sLinha = reader.readLine()) != null) {
                    String[] arrayLinha = sLinha.split("\\|");
                    
                    if (arrayLinha.length>2) {
                        if ((arrayLinha[1].equals("1990") && this.linhas.get(iLinha-1).contains("|1001|"))) {
                            this.linhas.add(iLinha, "|1010|N|N|N|N|N|N|N|N|N|");
                            listaC470.add(iLinha, "");
                            iLinha++;
                        }
                        if ((arrayLinha[1].equals("1001") && this.linhas.get(iLinha-1).contains("|H990|"))) {
                            this.linhas.add(iLinha, "|K001|1|");
                            listaC470.add(iLinha, "");
                            iLinha++;
                            this.linhas.add(iLinha, "|K990|2|");
                            listaC470.add(iLinha, "");
                            iLinha++;
                        }                        
                        if ((arrayLinha[1].equals("9900") && !arrayLinha[2].equals("K001")) && this.linhas.get(iLinha-1).contains("|9900|H990|")) {
                            this.linhas.add(iLinha, "|9900|K001|1|");
                            listaC470.add(iLinha, "");
                            iLinha++;
                            this.linhas.add(iLinha, "|9900|K990|1|");
                            listaC470.add(iLinha, "");
                            iLinha++;
                        }
                    }
                    
                    if (!sLinha.substring(0, 1).equals("|"))
                        this.linhas.add("");
                    else
                        this.linhas.add(iLinha, sLinha);

                    if (sLinha.contains("|C470|")) {
                        String[] array = sLinha.split("\\|", -1);
                        listaC470.add(iLinha, array[2]);
                    } else
                        listaC470.add(iLinha, "");
                    
                    iLinha++;
                }
                
                if (this.linhas != null) {
                    progressBar.setMaximum(this.linhas.size());

                    for (int i=0; i<this.linhas.size(); i++) {
                        //atualiza barra de progresso
                        progressBar.setValue(i);
                        progressBar.setString(i+"%");
                        percentProgress = (int) (((float) i/linhas.size())*100);
                        lblProgresso.setText("Processando arquivo na linha ["+i+"] - "+percentProgress+"%");

                        //substitui códigos de tributação
                        this.linhas.set(i, this.linhas.get(i).replace("\\|060\\|5102|", "|060|5405|"));
                        this.linhas.set(i, this.linhas.get(i).replace("\\|060\\|5929|", "|060|5405|"));
                        this.linhas.set(i, this.linhas.get(i).replace("\\|017\\|5102|", "|000|5102|"));
                        this.linhas.set(i, this.linhas.get(i).replace("\\|017\\|5929|", "|000|5102|"));
                        this.linhas.set(i, this.linhas.get(i).replace("\\|040\\|5929|", "|040|5102|"));
                        this.linhas.set(i, this.linhas.get(i).replace("\\|041\\|5929|", "|041|5102|"));
                        this.linhas.set(i, this.linhas.get(i).replace("\\|0FF\\|5102|", "|060|5405|"));
                        this.linhas.set(i, this.linhas.get(i).replace("\\|0NN\\|5102|", "|090|5102|"));
                        this.linhas.set(i, this.linhas.get(i).replace("\\|0II\\|5102|", "|041|5102|"));
                        
                        aLinha = this.linhas.get(i).split("\\|", -1);
                        
                        if (aLinha.length<2)
                            break;
                        else if (aLinha[1].equals("9999")) {
                            aLinha[2] = iContTotal+"";
                        } else if (aLinha[1].equals("0000")) {
                            aLinha[2] = "011";
                            aLinha[10] = aLinha[10].replaceAll("[^0-9]", "");
                            aLinha[11] = codMunicipio;
                        } else if (aLinha[1].equals("0200")) {
                            if (aLinha.length<15) {
                                aLinha = (this.linhas.get(i)+"|").split("\\|", -1);
                            }

                            int iPos = listaC470.indexOf(aLinha[2]);
                            if (iPos == -1) {
                                this.linhas.remove(i);
                                i--;
                                continue;
                            }
                            
                            iCont0200++;
                        } else if (aLinha[1].equals("1990")) {
                            aLinha[2] = "3";
                        } else if (aLinha[1].equals("9900")) {
                            iCont9900++;
                        }
                        
                        if (aLinha[1].equals("0990"))
                            aLinha[2] = iCont0200+"";
                        
                        if (aLinha[1].equals("9900") && aLinha[2].equals("9900")) {
                            iCont9900 = iCont9900+1;
                            aLinha[3] = iCont9900+"";
                        }
                        if (aLinha[1].equals("9990")) {
                            aLinha[2] = (iCont9900+1)+"";
                        }
                        
                        //ROTINA PARA VERIFICAR/CORRIGIR RATEIO DE DESCONTO POR NOTA
                        if (bVerificaEspelho) {
                            float valorCorrigido = 0;

                            if (bAtualizaValores && !aLinha[1].equals("C470"))
                                bAtualizaValores = false;
                            if (bAtualizaValores && aLinha[1].equals("C470")) {
                                for (ItensCupom item : cupomDesconto.getListaItens()) {
                                    if (item.getCodigo().equals(aLinha[2])) {
                                        aLinha[6] = String.format("%.2f", item.getValorBruto());
                                    }
                                }
                            }
                            if (aLinha[1].equals("C460")) {
                                for (CupomFiscal cupom : this.listaCupons) {
                                    if (cupom.getCoo() == Integer.parseInt(aLinha[4])) {
                                        if (cupom.getValorDesconto() > 0.0) {
                                            bAtualizaValores = true;

                                            ArrayList<ItensCupom> listaItens = cupom.getListaItens();

                                            cupomDesconto = cupom;
                                            cupomDesconto.setListaItens(new ArrayList<>());

                                            float valorDesconto = cupom.getValorDesconto();
                                            float percentDesconto = 0;
                                            float descontoProduto = 0;

                                            ArrayList<ItensCupom> itens = new ArrayList<>();
                                            for (ItensCupom item : listaItens) {
                                                //calcula a proporção do desconto para cada produto
                                                percentDesconto = (item.getValorBruto()*100)/cupom.getValorSubTotal();
                                                descontoProduto = (valorDesconto*percentDesconto)/100;
                                                valorCorrigido = item.getValorBruto()-descontoProduto;

                                                item.setValorBruto(valorCorrigido);
                                                itens.add(item);
                                            }

                                            cupomDesconto.setListaItens(itens);
                                        } else
                                            bAtualizaValores = false;
                                        break;
                                    }
                                }
                            } 
                        }
                        
                        iContTotal++;
                        
                        String linhaAtualizada="";
                        for (int j=0; j<aLinha.length; j++)
                            linhaAtualizada += "|"+aLinha[j].trim();
                        
                        //retira o primeiro "|"
                        linhaAtualizada = linhaAtualizada.substring(1);
                        
                        this.linhas.set(i, linhaAtualizada);

                        writer.write(this.linhas.get(i));
                        writer.newLine();
                    }

                    lblProgresso.setText("Processo concluído com sucesso!");
                    JOptionPane.showMessageDialog(this, "Processo concluído com sucesso!", "INFORMAÇÃO", JOptionPane.INFORMATION_MESSAGE);
                }
            
                reader.close();
                inputStream.close();
                writer.close();
                outputStream.close();
            } catch (IOException ex) {
                msg.mensagem("ERRO GRAVE", "Erro ao processar arquivo", ex.getMessage());
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JDOMException ex) {
                msg.mensagem("ERRO GRAVE", "Erro ao ler arquivo xml", ex.getMessage());
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                msg.mensagem("ERRO GRAVE", "Erro ao processar arquivo", ex.getMessage());
                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Arquivo Origem não encontrado!", "ATENÇÃO", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnProcessarActionPerformed

    private void btnEspelhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEspelhoActionPerformed
        if (procuraArqEspelho.showDialog(this, "Selecionar") != JFileChooser.APPROVE_OPTION) {
            return;
        }
        
        arquivoEspelho = procuraArqEspelho.getSelectedFile();
        
        txtEspelho.setText(arquivoEspelho.toString());
    }//GEN-LAST:event_btnEspelhoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDestino;
    private javax.swing.JButton btnEspelho;
    private javax.swing.JButton btnOrigem;
    private javax.swing.JButton btnProcessar;
    private javax.swing.JComboBox<String> comboMunicipio;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel lblProgresso;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JTextField txtDestino;
    private javax.swing.JTextField txtEspelho;
    private javax.swing.JTextField txtOrigem;
    // End of variables declaration//GEN-END:variables
}
